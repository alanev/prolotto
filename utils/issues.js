var glob = require('glob');
var path = require('path');
var fs = require('fs');

var tmpl = String(fs.readFileSync(__dirname + '/issue.md'));

glob('modules/*', function (err, files) {
    files.forEach(function (file, index) {
        var module = file.replace('modules/', '');
        var name = module + '.issues.md';
        var doc = tmpl.replace('{{name}}', module);
        
        fs.writeFileSync(path.join(file, name), doc);
        
        console.log(module, doc);
    });
});

console.log('done');

module.exports = {};
###Command
####server
Запускает сервер для собранной папки с портом указаным в config.json
####watch
Тоже что и server + отслеживает изменения файлов и запускает соответствующие задачи
####dev
Тоже что и wacth + плюс открывает сервер в firefox и папку с проектом в проводнике
####build
Команда для сборки
####img
Копирует изображения из всех модулей, минифицирует и создает webp
####sprite
Создает спрайт из изображений с префиксом 'i-' из всех модулей.
Созданный спрайт и стили копирует в модуль i-icons
####html
Собирает html всех страниц из папки 'pages'
####css
Собирает все стили модулей
####js
Собирает все скрипты модулей
####upload:all
Команда для загрузки файлов по ftp
Конфигурационный файл fpt.json в формате
```json
{
	"host": "hostname",
	"user": "username",
	"pass": "password"
}
```
####upload:archive
Создает два архива (с собранными файлами и с разрабатываевыми) и отправляет на серевер по ftp с настройками из ftp.json

###Postcss
####css syntax
+ css variables https://github.com/MadLittleMods/postcss-css-variables
+ media variables https://github.com/postcss/postcss-custom-media
+ color funcion https://github.com/postcss/postcss-color-function
+ not selector https://github.com/postcss/postcss-selector-not
####css shortcuts
+ nested https://github.com/postcss/postcss-nested
+ short https://github.com/jonathantneal/postcss-short
+ focus https://github.com/postcss/postcss-focus
####css optimization
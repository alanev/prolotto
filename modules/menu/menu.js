(function () {
	$('.menu__sandwich').click(function(){
		$('.menu__content, .menu__mobile').toggleClass('_active');
        return false;
	});
    $('.menu__mobile').click(function(){
        
        return false;
    });
    $(document).click(function () {
        $('.menu__mobile').removeClass('_active');
    });
    
    
    
    
	$('.menu__close').click(function(){
		$('.menu__content').removeClass('_active');
	});
	$('.menu__list__head').click(function(){
		$(this).parent().toggleClass('_active');
	});
	$('.menu__mobile__item').click(function(){
		$(this).toggleClass('_open');
		$(this).children('.menu__mobile__list').toggleClass('_open');
	});
	$('.menu__list__item').click(function(){
	   $(this).addClass('_active').siblings().removeClass('_active');
	   $('.menu__main__item').removeClass('_active').eq($(this).index()).addClass('_active');
	});
})();
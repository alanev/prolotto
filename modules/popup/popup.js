;NodeList.prototype.forEach = Array.prototype.forEach;
;(function () {
	var popup = {
		activeClass: '_active',
		closers: document.querySelectorAll('.popup__overlay, .popup__close'),
		open: function (popupName) {
			this.close();
			this.active = document.querySelector(`[data-popup=${popupName}]`);
			if (this.active) {
				this.active.classList.add(this.activeClass);
                document.body.classList.add('_lock');
			}
		},
		close: function () {
			if (this.active) {
				this.active.classList.remove(this.activeClass);
                document.body.classList.remove('_lock');
			}
		},
		openByHash: function () {
			var name = location.hash.replace('#popup/', '');
			if (name != '') {
				this.open(name);
			} else {
				this.close();
			}
		},
		init: function () {
			popup.openByHash();
			window.addEventListener('hashchange', function () {
				popup.openByHash();
			});
			this.closers.forEach(function (item) {
				item.addEventListener('click', function () {
					popup.close();
                    location.hash = '';
                    console.log('closed');
				});
			});
		}
	};
	if (popup.closers) {
		popup.init();
	}
})();
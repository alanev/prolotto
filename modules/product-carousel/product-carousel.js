$('.product-carousel').owlCarousel({
    loop:true,
    items: 1,
    nav:true,
    navText: ["<span class='product-carousel__arrow product-carousel__arrow_left'><img src='img/arrow-product.png' alt=''></span>","<span class='product-carousel__arrow product-carousel__arrow_right'><img src='img/arrow-product.png' alt=''></span>"],
    dots: false
})